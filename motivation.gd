@tool
extends EditorPlugin

# -- imports --
@onready var pjson: Resource = preload("res://addons/PRE.Motivate/putil/pjson.gd").new()

# -- vars --
var plugin_path: String = "res://addons/PRE.Motivate"
var base_quotes_path: String = "%s/quotes/base_quotes.json" % [plugin_path]

var dock_panel: Control = preload("scenes/motivate_panel/motivate_panel.tscn").instantiate() 
var dock_panel_default_slot: DockSlot = DOCK_SLOT_LEFT_BR
var quote_label: Label


# -- enter - exit --
func _enter_tree():

	randomize()
	
	# attach dock panel to the editor dock
	self.add_control_to_dock(dock_panel_default_slot, dock_panel)

	# grab refresh button and hook up signal
	var refresh_button: Button = dock_panel.get_node("Margin/VBox/RefreshButton")
	refresh_button.pressed.connect(refresh_quote)

	quote_label = dock_panel.get_node("Margin/VBox/Quote")


func _exit_tree():

	# cleanup
	self.remove_control_from_docks(dock_panel)
	dock_panel.free()


# -- functionality --

func _ready() -> void:

	# Set initial quote
	self.refresh_quote()


func refresh_quote() -> void:

	# load quotes from json file
	var quotes_json = pjson.load_json_from_file(base_quotes_path)

	# count quotes
	var quote_count: int = 0
	for i in quotes_json:
		quote_count += 1
	
	# generate random number
	if quote_count <= 0:
		push_error("PRE.Motivate [ERROR] > Failed to load quotes, quote count 0")
		return
	
	var rand_quote_num: int = (randi() % quote_count)
	
	# extract quote and author from quotes json data using random number
	var final_quote = quotes_json[str(rand_quote_num)]["quote"] as String
	var final_quote_author = quotes_json[str(rand_quote_num)]["author"] as String
	
	# change quote label text to new quote
	quote_label.text = "\"%s\" - %s" % [final_quote, final_quote_author]