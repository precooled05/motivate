extends Resource


func load_json_from_file(_path_to_file: String) -> Dictionary:

	# cheers to https://www.reddit.com/r/godot/comments/116nd15/tutorial_read_a_json_file_in_godot_4_rc2/ for this one

	var file: FileAccess = FileAccess.open(_path_to_file, FileAccess.READ)
	var content: String = file.get_as_text()
	var json: JSON = JSON.new()
	var parsed := json.parse_string(content)

	return parsed
