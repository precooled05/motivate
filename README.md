-- Motivate --

A simple plugin that displays a randomly picked motivational quote for when you're feeling lazy.

I don't expect anyone to actually use this, i only really made it to practice making plugins.

Inspired by Words Of Encouragement (Roblox Studio plugin)


-- Install --

\- Create or open a godot project, and create the `res://addons` if it does not exist <br />
\- In a terminal, cd to `res://addons` <br />
\- Run this command: `git clone https://gitlab.com/precooled05/motivate.git PRE.Motivate` <br />
\- Go to project settings > plugins and enable the plugin <br />


-- Uninstall --

\- Delete the `PRE.Motivate` directory from `res://addons` <br />